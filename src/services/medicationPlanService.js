import http from "./httpService";
import { apiUrl } from "../config.json";

const apiEndpoint = apiUrl + "/medicationPlans";

function medicationPlanUrl(id) {
  return `${apiEndpoint}/${id}`;
}

export function getMedicationPlans() {
  return http.get(apiEndpoint);
}

export function getMedicationPlan(id) {
  return http.get(medicationPlanUrl(id));
}

export function saveMedicationPlan(medicationPlan) {
  if (medicationPlan.id) {
    const body = { ...medicationPlan };
    delete body.id;
    return http.put(medicationPlanUrl(medicationPlan.id), body);
  }

  return http.post(apiEndpoint, medicationPlan);
}

export function deleteMedicationPlan(id) {
  return http.delete(medicationPlanUrl(id));
}
