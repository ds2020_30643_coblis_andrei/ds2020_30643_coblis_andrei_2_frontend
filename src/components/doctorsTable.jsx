import React, { Component } from "react";
//import auth from "../services/authService";
import { Link } from "react-router-dom";
import Table from "./common/table";

class DoctorsTable extends Component {
  columns = [
    {
      path: "username",
      label: "Username",
      content: doctor => <Link to={`/doctors/${doctor.id}`}>{doctor.username}</Link>
    },
  ];

  deleteColumn = {
    key: "delete",
    content: doctor => (
      <button
        onClick={() => this.props.onDelete(doctor)}
        className="btn btn-danger btn-sm"
      >
        Delete
      </button>
    )
  };

  constructor() {
    super();
    //const user = auth.getCurrentUser();
    //if (user && user.isAdmin) this.columns.push(this.deleteColumn);
    this.columns.push(this.deleteColumn);
  }

  render() {
    const { doctors, onSort, sortColumn } = this.props;

    return (
      <Table
        columns={this.columns}
        data={doctors}
        sortColumn={sortColumn}
        onSort={onSort}
      />
    );
  }
}

export default DoctorsTable;
