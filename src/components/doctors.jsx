import React, { Component } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import Pagination from "./common/pagination";
import DoctorsTable from "./doctorsTable";
import { getDoctors, deleteDoctor } from "../services/doctorService";
import { paginate } from "../utils/paginate";
import _ from "lodash";
import SearchBox from "./searchBox";

class Doctors extends Component {
    state = {
        doctors: [],
        currentPage: 1,
        pageSize: 4,
        searchQuery: "",
        sortColumn: { path: "username", order: "asc" }
    }

    async componentDidMount() {
        const { data: doctors } = await getDoctors();
        this.setState({ doctors });
    }

    handleDelete = async doctor => {
        const originalDoctors = this.state.doctors;
        const doctors = originalDoctors.filter(u => u.id !== doctor.id);
        this.setState({ doctors });
    
        try {
          await deleteDoctor(doctor.id);
        } catch (ex) {
          if (ex.response && ex.response.status === 404)
            toast.error("This doctor has already been deleted.");
    
          this.setState({ doctors: originalDoctors });
        }
    };

    handlePageChange = page => {
        this.setState({ currentPage: page });
    };

    handleSearch = query => {
        this.setState({ searchQuery: query, currentPage: 1 });
    };

    handleSort = sortColumn => {
        this.setState({ sortColumn });
    };

    getPagedData = () => {
        const {
          pageSize,
          currentPage,
          sortColumn,
          searchQuery,
          doctors: allDoctors
        } = this.state;
    
        let filtered = allDoctors;
        if (searchQuery)
          filtered = allDoctors.filter(u =>
            u.username.toLowerCase().startsWith(searchQuery.toLowerCase())
          );
    
        const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);
    
        const doctors = paginate(sorted, currentPage, pageSize);
    
        return { totalCount: filtered.length, data: doctors };
    };

    render() {
        const { length: count } = this.state.doctors;
        const { pageSize, currentPage, sortColumn, searchQuery } = this.state;
        //const { user } = this.props;
    
        if (count === 0) return <p>There are no doctors in the database.</p>;
    
        const { totalCount, data: doctors } = this.getPagedData();
    
        return (
          <div className="row">
            <div className="col">
              {
                <Link
                  to="/doctors/new"
                  className="btn btn-primary"
                  style={{ marginBottom: 20 }}
                >
                  New Doctor
                </Link>
              }
              <p>Showing {totalCount} doctors in the database.</p>
              <SearchBox value={searchQuery} onChange={this.handleSearch} />
              <DoctorsTable
                doctors={doctors}
                sortColumn={sortColumn}
                onDelete={this.handleDelete}
                onSort={this.handleSort}
              />
              <Pagination
                itemsCount={totalCount}
                pageSize={pageSize}
                currentPage={currentPage}
                onPageChange={this.handlePageChange}
              />
            </div>
          </div>
        );
    }

}
export default Doctors;