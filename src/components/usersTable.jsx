import React, { Component } from "react";
//import auth from "../services/authService";
import { Link } from "react-router-dom";
import Table from "./common/table";

class UsersTable extends Component {
  columns = [
    {
      path: "username",
      label: "Username",
      content: user => <Link to={`/users/${user.userId}`}>{user.username}</Link>
    },
    { path: "role", label: "Role" },
    { path: "firstName", label: "FirstName" },
    { path: "lastName", label: "LastName" },
    { path: "address", label: "Address"},
    { path: "gender", label: "Gender"},
    { path: "birthDate", label: "Birthdate"}
  ];

  deleteColumn = {
    key: "delete",
    content: user => (
      <button
        onClick={() => this.props.onDelete(user)}
        className="btn btn-danger btn-sm"
      >
        Delete
      </button>
    )
  };

  constructor() {
    super();
    //const user = auth.getCurrentUser();
    //if (user && user.isAdmin) this.columns.push(this.deleteColumn);
    this.columns.push(this.deleteColumn);
  }

  render() {
    const { users, onSort, sortColumn } = this.props;

    return (
      <Table
        columns={this.columns}
        data={users}
        sortColumn={sortColumn}
        onSort={onSort}
      />
    );
  }
}

export default UsersTable;
